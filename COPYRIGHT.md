﻿Copyright © 1999 - 2017, eZ Systems, AS. All Rights Reserved.

This file is part of eZ Publish Legacy : Projects Mirror.

eZ Publish Legacy : Projects Mirror code is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

eZ Publish Legacy : Projects Mirror is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with eZ Publish Legacy : Projects Mirror in [LICENSE.md](LICENSE.md).

If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).

# Text Documentation Content License

eZ Publish Legacy : Projects Mirror text content is also licensed under the GNU Free Documentation License.

The complete license agreement is included in the [LICENSE_CONTENT.md](LICENSE_CONTENT.md) file.

Permission is granted to copy, distribute and/or modify this document under the terms of the GNU Free Documentation License,Version 1.2 or any later version published by the Free Soft- ware Foundation; with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts. A copy of the license can be downloaded from https://www.gnu.org/licenses/old-licenses/fdl-1.2.html

Corrections and/or suggestions might be sent to info@ezpublishlegacy.com

It is also available at [https://www.gnu.org/licenses/old-licenses/fdl-1.2.txt](https://www.gnu.org/licenses/old-licenses/fdl-1.2.txt)

You should have received a copy of the GNU Free Documentation License v1.2
along with eZ Publish Legacy : Documentation in the [LICENSE](LICENSE.md) file.

If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).

Using eZ Publish Legacy : Documentation under the terms of the GNU GFDL is free (as in freedom).
