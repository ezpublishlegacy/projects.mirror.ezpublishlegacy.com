eZ Publish Legacy : Projects Mirror
===================================

This repository provides a complete and accurate mirror static html of the website http:/projects.ez.no

Version
=======

* The current version of eZ Publish Legacy : Projects Mirror is 0.1.2

* Last Major update: February 7, 2017


Copyright
=========

* eZ Publish Legacy : Projects Mirror is copyright 1999 - 2017 eZ Systems, AS.

* See: [COPYRIGHT.md](COPYRIGHT.md) for more information on the terms of the copyright and license


Licenses
========


Code License
=============

eZ Publish Legacy : Projects Mirror is licensed under the GNU General Public License.

The complete license agreement is included in the [LICENSE.md](LICENSE.md) file.


eZp Order Report is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License or at your
option a later version.

eZp Order Report is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

The GNU GPL gives you the right to use, modify and redistribute
eZp Order Report under certain conditions. The GNU GPL license
is distributed with the software, see the file doc/LICENSE.

It is also available at [http://www.gnu.org/licenses/gpl.txt](http://www.gnu.org/licenses/gpl.txt)

You should have received a copy of the GNU General Public License
along with eZp Order Report in the file [LICENSE.md](LICENSE.md).

If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).

Using eZ Publish Legacy : Projects Mirror under the terms of the GNU GFDL is free (as in freedom).

For more information or questions please contact: license@brookinsconsulting.com


Documentation License
=====================

eZ Publish Legacy : Projects Mirror is licensed under the GNU Free Documentation License.

The complete license agreement is included in the [LICENSE_CONTENT.md](LICENSE_CONTENT.md) file.

Permission is granted to copy, distribute and/or modify this document under the terms of the GNU Free Documentation License,Version 1.2 or any later version published by the Free Soft- ware Foundation; with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts. A copy of the license can be downloaded from https://www.gnu.org/licenses/old-licenses/fdl-1.2.html

Corrections and/or suggestions might be sent to info@ezpublishlegacy.com

It is also available at [http://www.gnu.org/licenses/gpl.txt](http://www.gnu.org/licenses/gpl.txt)

You should have received a copy of the GNU Free Documentation License v1.2
along with eZ Publish Legacy : Projects Mirror in the [LICENSE_CONTENT.md](LICENSE_CONTENT.md) file.

If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).

Using eZ Publish Legacy : Projects Mirror under the terms of the GNU GFDL is free (as in freedom).

For more information or questions please contact: license@brookinsconsulting.com


Features
========

This repository provides the following features:

* Static HTML Mirror of the http://projects.ez.no website for for eZ Publish 3.x -> 6.x related code, extensions and documentation.


Usage
=====

Simply visit our publicly hosted mirror of this repository at http://projects.mirror.ezpublishlegacy.com


Troubleshooting
===============

### Support

If you have find any problems not handled by this document you can contact Brookins Consulting through the support system: [http://brookinsconsulting.com/contact](http://brookinsconsulting.com/contact)

